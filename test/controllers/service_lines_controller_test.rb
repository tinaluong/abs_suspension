require 'test_helper'

class ServiceLinesControllerTest < ActionController::TestCase
  setup do
    @service_line = service_lines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_lines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_line" do
    assert_difference('ServiceLine.count') do
      post :create, service_line: { Employee_id: @service_line.Employee_id, Part_id: @service_line.Part_id, ServiceStatus_id: @service_line.ServiceStatus_id, Service_id: @service_line.Service_id, Ticket_id: @service_line.Ticket_id, hour: @service_line.hour, note: @service_line.note }
    end

    assert_redirected_to service_line_path(assigns(:service_line))
  end

  test "should show service_line" do
    get :show, id: @service_line
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service_line
    assert_response :success
  end

  test "should update service_line" do
    patch :update, id: @service_line, service_line: { Employee_id: @service_line.Employee_id, Part_id: @service_line.Part_id, ServiceStatus_id: @service_line.ServiceStatus_id, Service_id: @service_line.Service_id, Ticket_id: @service_line.Ticket_id, hour: @service_line.hour, note: @service_line.note }
    assert_redirected_to service_line_path(assigns(:service_line))
  end

  test "should destroy service_line" do
    assert_difference('ServiceLine.count', -1) do
      delete :destroy, id: @service_line
    end

    assert_redirected_to service_lines_path
  end
end
