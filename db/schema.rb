# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150702223337) do

  create_table "categories", force: true do |t|
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customers", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.integer  "zip"
    t.string   "phone"
    t.string   "email"
    t.string   "additional_contact"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parts", force: true do |t|
    t.string   "part"
    t.decimal  "price",      precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "service_lines", force: true do |t|
    t.decimal  "hour",             precision: 10, scale: 0
    t.string   "note"
    t.integer  "Category_id"
    t.integer  "Ticket_id"
    t.integer  "Service_id"
    t.integer  "ServiceStatus_id"
    t.integer  "Employee_id"
    t.integer  "Part_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "service_lines", ["Category_id"], name: "index_service_lines_on_Category_id", using: :btree
  add_index "service_lines", ["Employee_id"], name: "index_service_lines_on_Employee_id", using: :btree
  add_index "service_lines", ["Part_id"], name: "index_service_lines_on_Part_id", using: :btree
  add_index "service_lines", ["ServiceStatus_id"], name: "index_service_lines_on_ServiceStatus_id", using: :btree
  add_index "service_lines", ["Service_id"], name: "index_service_lines_on_Service_id", using: :btree
  add_index "service_lines", ["Ticket_id"], name: "index_service_lines_on_Ticket_id", using: :btree

  create_table "service_statuses", force: true do |t|
    t.string   "status"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "services", force: true do |t|
    t.string   "service"
    t.string   "position"
    t.string   "type"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "services", ["category_id"], name: "index_services_on_category_id", using: :btree

  create_table "ticket_statuses", force: true do |t|
    t.string   "status"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tickets", force: true do |t|
    t.string   "note"
    t.integer  "vehicle_id"
    t.integer  "TicketStatus_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "customer_name"
  end

  add_index "tickets", ["TicketStatus_id"], name: "index_tickets_on_TicketStatus_id", using: :btree
  add_index "tickets", ["vehicle_id"], name: "index_tickets_on_vehicle_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vehicles", force: true do |t|
    t.integer  "year"
    t.string   "make"
    t.string   "model"
    t.decimal  "GVW",         precision: 10, scale: 0
    t.string   "license"
    t.decimal  "miles",       precision: 10, scale: 0
    t.string   "motor"
    t.string   "VIN"
    t.integer  "customer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "vehicles", ["customer_id"], name: "index_vehicles_on_customer_id", using: :btree

end
