# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.delete_all
Customer.delete_all
Employee.delete_all
Service.delete_all
TicketStatus.delete_all
Vehicle.delete_all



Category.create(category:"Brakes")
Category.create(category:"Suspension")

Customer.create(first_name:"John", last_name:"Doe", address:"123 abc", city:"Houston", state:"TX", phone:"1234567890", email:"jd@mail.com")
Customer.create(first_name:"Jane", last_name:"Moe", address:"321 cba", city:"Houston", state:"TX", phone:"7131234567", email:"jm@mail.com")

Employee.create(first_name:"John", last_name:"Bates", position:"owner", phone:"7130981209")
Employee.create(first_name:"Matt", last_name:"Fierro", position:"technician", phone:"8320981920")
Employee.create(first_name:"Billy", last_name:"Koslosky", position:"technician", phone:"7132374859")

Service.create(category_id:"1", service:"Rotors")
Service.create(category_id:"1", service:"Break Pads")
Service.create(category_id:"2", service:"Springs")
Service.create(category_id:"2", service:"Coils")

TicketStatus.create(status:"Pending")
TicketStatus.create(status:"Waiting on parts")
TicketStatus.create(status:"Completed")

Vehicle.create(VIN:"111111", customer_id:"1", license:"CV8F0928", year:"1995", make:"Ford", model:"Mustang", motor:"5.0")
Vehicle.create(VIN:"222222", customer_id:"2", license:"VDL1928", year:"1997", make:"Subaru", model:"WRX", motor:"2.0")