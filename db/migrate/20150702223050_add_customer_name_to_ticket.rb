class AddCustomerNameToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :customer_name, :string
  end
end
