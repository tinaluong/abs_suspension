class CreateServiceStatuses < ActiveRecord::Migration
  def change
    create_table :service_statuses do |t|
      t.string :status
      t.string :description

      t.timestamps
    end
  end
end
