class CreateServiceLines < ActiveRecord::Migration
  def change
    create_table :service_lines do |t|
      t.decimal :hour
      t.string :note
      t.references :Category, index:true
      t.references :Ticket, index: true
      t.references :Service, index: true
      t.references :ServiceStatus, index: true
      t.references :Employee, index: true
      t.references :Part, index: true

      t.timestamps
    end
  end
end
