json.array!(@service_statuses) do |service_status|
  json.extract! service_status, :id, :status, :description
  json.url service_status_url(service_status, format: :json)
end
