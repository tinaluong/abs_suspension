json.extract! @customer, :id, :first_name, :last_name, :address, :city, :state, :zip, :phone, :email, :additional_contact, :created_at, :updated_at
