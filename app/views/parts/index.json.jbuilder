json.array!(@parts) do |part|
  json.extract! part, :id, :part, :price
  json.url part_url(part, format: :json)
end
