class Employee < ActiveRecord::Base
  has_many :service_lines

  def full_name
    "#{last_name}, #{first_name}"
  end
end
