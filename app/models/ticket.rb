class Ticket < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :ticket_status
  has_many :service_lines
  accepts_nested_attributes_for :service_lines, allow_destroy: true

end
