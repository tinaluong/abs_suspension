class Category < ActiveRecord::Base
  has_many :services
  has_many :service_lines

  def cat
    "#{category}"
  end
end
