class Customer < ActiveRecord::Base
  has_many :vehicles

  validates_presence_of :last_name
  validates_presence_of :first_name
  validates_presence_of :phone
  validates :phone, length: { minimum: 10, maximum: 12 }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, }, :allow_blank => true

  def full_name
    "#{last_name}, #{first_name}"
  end
end

