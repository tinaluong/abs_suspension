class ServiceLine < ActiveRecord::Base
  belongs_to :ticket
  belongs_to :service
  belongs_to :service_status
  belongs_to :employee
  belongs_to :part
  belongs_to :category
end
