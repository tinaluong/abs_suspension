class Service < ActiveRecord::Base
  has_many :service_lines
  belongs_to :category
  belongs_to :ticket
  self.inheritance_column = nil
end
