class Vehicle < ActiveRecord::Base
  belongs_to :customer
  has_many :tickets

  validates_presence_of :make
  validates_presence_of :model
  validates_presence_of :license
  validates_presence_of :VIN

  def vehicle_info
    "#{make}, #{model}, #{license}"
  end
end

