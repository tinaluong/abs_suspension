class ServiceLinesController < ApplicationController
  before_action :set_service_line, only: [:show, :edit, :update, :destroy]

  # GET /service_lines
  # GET /service_lines.json
  def index
    @service_lines = ServiceLine.all
  end

  # GET /service_lines/1
  # GET /service_lines/1.json
  def show
  end

  # GET /service_lines/new
  def new
    @ticket = Ticket.find(params[:ticket_id])
    @service_line = ServiceLine.new
  end

  # GET /service_lines/1/edit
  def edit
  end

  # POST /service_lines
  # POST /service_lines.json
  def create
    @service_line = ServiceLine.new(service_line_params)

    respond_to do |format|
      if @service_line.save
        format.html { redirect_to @service_line, notice: 'Service line was successfully created.' }
        format.json { render :show, status: :created, location: @service_line }
      else
        format.html { render :new }
        format.json { render json: @service_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_lines/1
  # PATCH/PUT /service_lines/1.json
  def update
    respond_to do |format|
      if @service_line.update(service_line_params)
        format.html { redirect_to @service_line, notice: 'Service line was successfully updated.' }
        format.json { render :show, status: :ok, location: @service_line }
      else
        format.html { render :edit }
        format.json { render json: @service_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_lines/1
  # DELETE /service_lines/1.json
  def destroy
    @service_line.destroy
    respond_to do |format|
      format.html { redirect_to service_lines_url, notice: 'Service line was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_line
      @service_line = ServiceLine.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_line_params
      params.require(:service_line).permit(:hour, :note, :Ticket_id, :Service_id, :ServiceStatus_id, :Employee_id, :Part_id)
    end
end
